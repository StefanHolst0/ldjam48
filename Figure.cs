using System;
using System.Collections.Generic;
using System.Diagnostics;
using Terminal.Gui;

namespace ldjam
{
    public class Figure : View
    {
        Dictionary<string, string> figures = new Dictionary<string, string>();
        string hOrientation = "right";
        string vOrientation = "";
        private Direction direction { get; set; } = new Direction();
        public Action<Point> FigureMoved;

        public Figure()
        {
            Height = 3;
            Width = 8;
            CanFocus = true;

            figures["right"] =
                @" ^_^    " +
                @"[###]//>" +
                @"(:::)   ";
            figures["left"] =
                @"    ^_^ " +
                @"<\\[###]" +
                @"   (:::)";
            figures["rightdown"] =
                @" ^_^    " +
                @"[###]\\ " +
                @"(:::) V ";
            figures["rightup"] =
                @" ^_^  A " +
                @"[###]// " +
                @"(:::)   ";
            figures["leftdown"] =
                @"    ^_^ " +
                @" //[###]" +
                @" V (:::)";
            figures["leftup"] =
                @" A  ^_^ " +
                @" \\[###]" +
                @"   (:::)";
        }
        public override void PositionCursor()
        {
            Move(0, 0);
        }
        public override bool OnEnter(View view)
        {
            Application.Driver.SetCursorVisibility(CursorVisibility.Invisible);
            return base.OnEnter(view);
        }

        public void Reset()
        {
            x = 0;
            X = 0;
            y = 0;
            Y = 0;
            direction = new Direction();
            hOrientation = "right";
            vOrientation = "";
            SetNeedsDisplay();
        }

        public override bool OnKeyDown(KeyEvent keyEvent)
        {
            if (keyEvent.Key == Key.CursorUp)
            {
                direction.Y = -1;
                MoveFigure();
                return true;
            }

            if (keyEvent.Key == Key.CursorDown)
            {
                direction.Y = 1;
                MoveFigure();
                return true;
            }

            if (keyEvent.Key == Key.CursorLeft)
            {
                direction.X = -1;
                MoveFigure();
                return true;
            }

            if (keyEvent.Key == Key.CursorRight)
            {
                direction.X = 1;
                MoveFigure();
                return true;
            }

            return base.OnKeyDown(keyEvent);
        }
        public override bool OnKeyUp(KeyEvent keyEvent)
        {
            if (keyEvent.Key == Key.CursorUp || keyEvent.Key == Key.CursorDown)
            {
                direction.Y = 0;
                return true;
            }

            if (keyEvent.Key == Key.CursorLeft || keyEvent.Key == Key.CursorRight)
            {
                direction.X = 0;
                return true;
            }

            return base.OnKeyDown(keyEvent);
        }
        public override bool ProcessKey(KeyEvent keyEvent)
        {
            if (keyEvent.Key == Key.CursorUp ||
                keyEvent.Key == Key.CursorDown ||
                keyEvent.Key == Key.CursorLeft ||
                keyEvent.Key == Key.CursorRight)
                return true;

            return base.ProcessKey(keyEvent);
        }

        private int x = 0;
        private int y = 0;
        public void MoveFigure()
        {
            var newX = direction.X + x;
            var newY = direction.Y + y;
            var parentFrame = SuperView.Bounds;

            // Return if we are not moving
            if (direction.X == 0 && direction.Y == 0)
                return;

            // make sure we don't leave the parent
            if (newX != x && newX >= 0 && newX + Frame.Width <= parentFrame.Width)
                x = newX;
            if (newY != y && newY >= 0 && newY + Frame.Height <= parentFrame.Height)
                y = newY;

            // Update position
            X = x;
            Y = y;

            // Change figure based on direction
            if (direction.X > 0)
                hOrientation = "right";
            if (direction.X < 0)
                hOrientation = "left";
            if (direction.Y > 0)
                vOrientation =  "down";
            if (direction.Y < 0)
                vOrientation = "up";
            if (direction.Y == 0)
                vOrientation = "";

            CheckCollision();

            SetNeedsDisplay();

            FigureMoved?.Invoke(new Point(x, y));
        }

        void CheckCollision()
        {
            var startX = x + 1;
            var startY = y + 1;
            var endX = Frame.Width + startX - 1;
            var endY = Frame.Height + startY - 1;

            for (int y = startY; y <= endY; y++)
            {
                for (int x = startX; x <= endX; x++)
                {
                    // Check collision
                    var p = new Point(x, y);
                    var item = Map.Get(p);
                    if (item.HasValue)
                    {
                        // Collision
                        Map.Remove(p);
                        MainWindow.stats.UpdateStats(item.Value);
                    }
                }
            }
        }
        
        public override void Redraw(Rect bounds)
        {
            Driver.SetAttribute (ColorScheme.Normal);
            Clear();

            var figure = figures[hOrientation + vOrientation];

            for (int y = 0; y < Frame.Height; y++)
            {
                for (int x = 0; x < Frame.Width; x++)
                {
                    Move (x, y);
                    var test = figure[x + y * Frame.Width];
                    Driver.AddRune(test);
                }
            }
        }
    }

    class Direction
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}