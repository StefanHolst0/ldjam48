﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using NStack;
using Terminal.Gui;

namespace ldjam
{
    class Canvas : FrameView
    {
        private List<Figure> objects = new List<Figure>();
        ColorScheme rockScheme = new ColorScheme();
        ColorScheme silverScheme = new ColorScheme();
        ColorScheme diamondScheme =  new ColorScheme();
        ColorScheme bombScheme = new ColorScheme();
        ColorScheme dollarScheme = new ColorScheme();

        public Canvas()
        {
            rockScheme.Normal = Application.Driver.MakeAttribute(Color.Brown, Color.Blue);
            silverScheme.Normal = Application.Driver.MakeAttribute(Color.Gray, Color.Blue);
            diamondScheme.Normal = Application.Driver.MakeAttribute(Color.White, Color.Blue);
            bombScheme.Normal = Application.Driver.MakeAttribute(Color.BrightRed, Color.Blue);
            dollarScheme.Normal = Application.Driver.MakeAttribute(Color.BrightGreen, Color.Blue);

            Start();

            LayoutComplete += (e) =>
            {
                if (Map.Initiated == false)
                {
                    // Generate a map
                    Map.GenerateMap(e.OldBounds.Width, e.OldBounds.Height);
                }
            };

        }
        public void Start()
        {
            // Application.MainLoop.AddTimeout(TimeSpan.FromMilliseconds(200), Update);
        }

        public void Add(Figure view)
        {
            objects.Add(view);
            base.Add(view);
        }

        private bool Update(MainLoop caller)
        {
            foreach (var subview in objects)
            {
                subview.MoveFigure();
            }

            return true;
        }

        public override void Redraw(Rect bounds)
        {
            base.Redraw(bounds);

            Driver.SetAttribute(ColorScheme.Normal);

            for (int y = 0; y < Frame.Height; y++)
            {
                for (int x = 0; x < Frame.Width; x++) 
                {
                    var material = Map.Get(new Point(x, y));
                    if (material.HasValue)
                    {
                        Move(x, y);

                        switch (material)
                        {
                            case Material.Rock:
                                Driver.SetAttribute(rockScheme.Normal);
                                break;
                            case Material.Silver:
                                Driver.SetAttribute(silverScheme.Normal);
                                break;
                            case Material.Bomb:
                                Driver.SetAttribute(bombScheme.Normal);
                                break;
                            case Material.Diamonds:
                                Driver.SetAttribute(diamondScheme.Normal);
                                break;
                            case Material.Dollars:
                                Driver.SetAttribute(dollarScheme.Normal);
                                break;
                            default:
                                Driver.SetAttribute(ColorScheme.Normal);
                                break;
                        }

                        var rune = new Rune((char)material);
                        Driver.AddRune(rune);
                    }
                }
            }
        }
    }
}
