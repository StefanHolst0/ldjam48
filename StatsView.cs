using Terminal.Gui;

namespace ldjam
{
    public class StatsView : View
    {
        public int Money { get; set; } = 10;
        public double Load { get; set; } = 0.0;
        public int Health { get; set; } = 100;
        public double Wear { get; set; } = 1.0;

        private TextView MoneyView;
        private TextView LoadView;
        private TextView HealthView;
        private TextView WearView;

        private ColorScheme normalScheme;
        private ColorScheme yellowScheme;
        private ColorScheme redScheme;

        public StatsView()
        {
            normalScheme = new ColorScheme();
            normalScheme.Normal = Application.Driver.MakeAttribute(Color.White, Color.Blue);
            yellowScheme = new ColorScheme();
            yellowScheme.Normal = Application.Driver.MakeAttribute(Color.BrightYellow, Color.Blue);
            redScheme = new ColorScheme();
            redScheme.Normal = Application.Driver.MakeAttribute(Color.BrightRed, Color.Blue);

            var moneyLabel = new Label("Money: ")
            {
                Y = 0,
                X = 0,
                Width = 8
            };
            MoneyView = new TextView
            {
                Y = 0,
                X = Pos.Right(moneyLabel),
                Width = 10,
                Height = 1,
                CanFocus = false
            };
            Add(moneyLabel);
            Add(MoneyView);

            var LoadLabel = new Label("Load: ")
            {
                Y = 1,
                X = 0,
                Width = 8
            };
            LoadView = new TextView
            {
                Y = 1,
                X = Pos.Right(LoadLabel),
                Width = 10,
                Height = 1,
                CanFocus = false
            };
            Add(LoadLabel);
            Add(LoadView);


            var HealthLabel = new Label("Health: ")
            {
                Y = 2,
                X = 0,
                Width = 8
            };
            HealthView = new TextView
            {
                Y = 2,
                X = Pos.Right(HealthLabel),
                Width = 10,
                Height = 1,
                CanFocus = false
            };
            Add(HealthLabel);
            Add(HealthView);

            var WearLabel = new Label("Wear: ")
            {
                Y = 3,
                X = 0,
                Width = 8
            };
            WearView = new TextView
            {
                Y = 3,
                X = Pos.Right(WearLabel),
                Width = 10,
                Height = 1,
                CanFocus = false
            };
            Add(WearLabel);
            Add(WearView);
            
            UpdateViews();
        }
        
        public void UpdateStats(Material material)
        {
            switch (material)
            {
                case Material.Rock:
                    Load += 0.0001;
                    Wear -= 0.001;
                    Money += 1;
                    break;
                case Material.Silver:
                    Load += 0.01;
                    Money += 10;
                    break;
                case Material.Bomb:
                    Health -= 10;
                    Wear -= 0.1;
                    break;
                case Material.Diamonds:
                    Load += 0.001;
                    Money += 1000;
                    Wear -= 0.001;
                    break;
                case Material.Dollars:
                    Money += 100;
                    break;
            }

            UpdateViews();

            CheckViolations();
        }

        private void CheckViolations()
        {
            if (Load >= 1.0)
                Program.MainWindow.GameOver($"Your digger got crushed by the load!\nYou can retire with {Money} on your account.");
            else if (Wear <= 0.0)
                Program.MainWindow.GameOver($"Your digger broke down, you are forever stuck...\nYou can retire with {Money} on your account.");
            else if (Health <= 0)
                Program.MainWindow.GameOver($"Your digger blew up. You ded...\nYou can retire with {Money} on your account.");
        }

        private void UpdateViews()
        {
            // Set color for health
            if (Health <= 20)
                HealthView.ColorScheme = redScheme;
            else if (Health <= 50)
                HealthView.ColorScheme = yellowScheme;
            else if (Health > 50)
                HealthView.ColorScheme = normalScheme;

            // Set color for load
            if (Load >= 0.75)
                LoadView.ColorScheme = redScheme;
            else if (Load >= 0.5)
                LoadView.ColorScheme = yellowScheme;
            else if (Load < 0.5)
                LoadView.ColorScheme = normalScheme;

            // Set color for wear
            if (Wear <= 0.25)
                WearView.ColorScheme = redScheme;
            else if (Wear <= 0.5)
                WearView.ColorScheme = yellowScheme;
            else if (Wear > 0.5)
                WearView.ColorScheme = normalScheme;

            MoneyView.Text = Money.ToString();
            LoadView.Text = Load.ToString("P1");
            HealthView.Text = Health.ToString();
            WearView.Text = Wear.ToString("P1");
        }

        public void Reset()
        {
            Money = 10;
            Load = 0.0;
            Health = 100;
            Wear = 1.0;
            UpdateViews();
        }

        public void Sell()
        {
            Load = 0.0;
            UpdateViews();
        }
        public void Repair()
        {
            if (Money >= 1000)
            {
                Money -= 1000;
                Wear = 1.0;
                UpdateViews();
            }
        }
        public bool NextMission()
        {
            if (Money >= 10000)
            {
                Money -= 10000;
                return true;
            }

            return false;
        }
    }
}