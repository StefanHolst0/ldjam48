﻿using System;
using Terminal.Gui;

namespace ldjam
{
    class Program
    {
        public static MainWindow MainWindow { get; set; }
        
        static void Main(string[] args)
        {
            Console.CancelKeyPress += (sender, eventArgs) => { Application.Shutdown(); };
            
            Application.Init();
            var top = Application.Top;
            MainWindow = new MainWindow();
            MainWindow.Init();
            top.Add(MainWindow);
            
            try
            {
                Application.Run();
            }
            catch (Exception)
            {
            }
        }
    }
}
