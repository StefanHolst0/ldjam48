using Terminal.Gui;

namespace ldjam
{
    public class Shop : View
    {
        private string figure =
            @"   ______   " +
            @"  / SHOP \  " +
            @"_/        \_" +
            @" | □ Π  П | ";

        private Button sellButton;
        private Button repairButton;
        private Button nextButton;


        public Shop()
        {
            Height = 4;
            Width = 12 + 20;
            MainWindow.figure.FigureMoved += FigureMoved;

            sellButton = new Button("Offload");
            sellButton.X = 14;
            sellButton.Clicked += MainWindow.stats.Sell;
            Add(sellButton);

            repairButton = new Button("Repair (1000g)");
            repairButton.X = 14;
            repairButton.Y = 1;
            repairButton.Clicked += MainWindow.stats.Repair;
            Add(repairButton);

            nextButton = new Button("Next Mission (10000g)");
            nextButton.X = 14;
            nextButton.Y = 2;
            nextButton.Clicked += Program.MainWindow.NextMission;
            Add(nextButton);

            EnableShop(false);
        }
        
        public override void Redraw(Rect bounds)
        {
            base.Redraw(bounds);
            Driver.SetAttribute (ColorScheme.Normal);

            for (int y = 0; y < Frame.Height; y++)
            {
                for (int x = 0; x < 12; x++)
                {
                    Move (x, y);
                    var test = figure[x + y * 12];
                    Driver.AddRune(test);
                }
            }
        }

        void FigureMoved(Point p)
        {
            if (p.X > Frame.X - 2 && p.X < Frame.X + 2 && p.Y == 0)
                EnableShop(true);
            else
                EnableShop(false);
        }

        public void EnableShop(bool enabled)
        {
            sellButton.Visible = enabled;
            sellButton.CanFocus = enabled;
            repairButton.Visible = enabled;
            repairButton.CanFocus = enabled;
            nextButton.Visible = enabled;
            nextButton.CanFocus = enabled;
        }
    }
}