using Terminal.Gui;

namespace ldjam
{
    public class MainWindow :  Window
    {
        private StatsView _Stats;
        public static StatsView stats
        {
            get
            {
                return Program.MainWindow._Stats;
            }
            set
            {
                Program.MainWindow._Stats = value;
            }
        }
        private Figure _figure;
        public static Figure figure
        {
            get
            {
                return Program.MainWindow._figure;
            }
            set
            {
                Program.MainWindow._figure = value;
            }
        }
        private Shop _shop;
        public static Shop shop
        {
            get
            {
                return Program.MainWindow._shop;
            }
            set
            {
                Program.MainWindow._shop = value;
            }
        }

        public MainWindow()
        {
            // Set title
            Title = "Diiiiig!";
        }

        public void Init()
        {
            stats = new StatsView();
            figure = new Figure();
            shop = new Shop();

            // Add close button
            var closeButton = new Button("Close");
            closeButton.X = Pos.Right(this) - 12;
            closeButton.Clicked += () => { Application.Shutdown(); };
            Add(closeButton);

            // Add stats
            stats.X = 1;
            stats.Height = Dim.Fill();
            stats.Width = Dim.Fill();
            Add(stats);

            shop.X = Pos.Percent(50) - 8;
            Add(shop);

            // Add canvas
            var canvas = new Canvas();
            canvas.X = 1;
            canvas.Y = 4;
            canvas.Width = Dim.Fill();
            canvas.Height = Dim.Fill();
            canvas.Add(figure);
            Add(canvas);


            // Reset
            var resetButton = new Button("Reset");
            resetButton.Y = 2;
            resetButton.X = Pos.Right(this) - 12;
            resetButton.Clicked += () => { Reset(); };
            Add(resetButton);
        }

        public void NextMission()
        {
            if (stats.NextMission())
            {
                Map.Reset();
                figure.Reset();
                shop.EnableShop(false);
            }
        }

        public void Reset()
        {
            Map.Reset();
            figure.Reset();
            stats.Reset();
            shop.EnableShop(false);
        }

        public void GameOver(string message)
        {
            MessageBox.Query("Game Over!", message, "Try again.");
            Reset();
        }

        public override bool ProcessKey(KeyEvent keyEvent)
        {
            if (keyEvent.Key == Key.Esc)
                Application.Shutdown();

            return base.ProcessKey(keyEvent);
        }
    }
}