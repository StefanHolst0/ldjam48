﻿using System;
using System.Collections.Generic;
using Terminal.Gui;
 
namespace ldjam
{
    public enum Material
    {
        Rock = '+',
        Silver = 'X',
        Bomb = 'ó',
        Diamonds = '◊',
        Dollars = '$'
    }
    
    public static class Map
    {
        private static Random rand = new Random();
        private static int _height;
        private static int _width;
        private static Dictionary<Point, Material> map = new Dictionary<Point, Material>();
        public static bool Initiated = false;

        public static Material? Get(Point p)
        {
            if (map.ContainsKey(p))
                return map[p];

            return null;
        }

        public static void Remove(Point p)
        {
            if (map.ContainsKey(p))
                map.Remove(p);
        }

        public static void GenerateMap(int width, int height)
        {
            Initiated = true;
            _height = height;
            _width = width;

            var area = _height * _width;
            int radio = area / 1300;

            // Generate Silver
            var count = rand.Next(5 * radio, 15 * radio);
            for (int i = 0; i < count; i++)
                GenerateMaterial(Material.Silver, 0.5);
            
            // Generate bombs
            count = rand.Next(10 * radio, 20 * radio);
            for (int i = 0; i < count; i++)
                GenerateMaterial(Material.Bomb, 0.15);

            // Generate diamonds
            count = rand.Next(1 * radio, 3 * radio);
            for (int i = 0; i < count; i++)
                GenerateMaterial(Material.Diamonds, 0.2);

            // Generate dollars
            count = rand.Next(1 * radio, 5 * radio);
            for (int i = 0; i < count; i++)
                GenerateMaterial(Material.Dollars, 0.2);

            // Generate rock
            for (int y = 1; y < _height - 1; y++)
            {
                for (int x = 1; x < _width - 1; x++)
                {
                    if (x <= 8 && y <= 3)
                    {
                        Remove(new Point(x, y));
                        continue;
                    }
                    
                    var p = new Point(x, y);
                    if (map.ContainsKey(p) == false)
                        map[p] = Material.Rock;
                }
            }
        }

        static void GenerateMaterial(Material material, double chance)
        {
            var x = rand.Next(1, _width - 1);
            var y = rand.Next(1, _height - 1);
            var p = new Point(x, y);

            GenerateSurrounding(p, material, chance);
        }
        static void GenerateSurrounding(Point p, Material material, double chance)
        {
            map[p] = material;
            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (x == 0 && y == 0) // center
                        continue;

                    var p2 = new Point(p.X + x, p.Y + y);
                    if (p2.X < 1 || p2.X > _width - 2 || p2.Y < 1 || p2.Y > _height - 2) // out of bounds
                        continue;
                    
                    if (rand.NextDouble() < chance)
                        GenerateSurrounding(p2, material, chance / 2);
                }
            }
        }

        public static void Reset()
        {
            map.Clear();
            GenerateMap(_width, _height);
        }
    }
}
